#!/usr/bin/env python
# coding: utf-8

# In[2]:


from pathlib import Path

class IMMUNIZATION:
    __VaccineList=[] #list containing vaccine and strains
    __Edges=[[],[]] #matrix of edges/associations
    
    # To add input list into the graph
    def __addInput(self,lineList):
        for item in lineList:
            #if Item in list is a new edge i.e. new variant or vaccine
            if item not in self.__VaccineList:
                #append new edge in VaccineList
                self.__VaccineList.append(item)
                #item is Variant
                if item is lineList[0] :
                    #add variant as string in Edge[0] containing all variants
                    self.__Edges[0].append(item)
                    #add all the vaccines related to variant in Edge[1] containing all vaccines 
                    self.__Edges[1].append(lineList[1:])
                #item is Vaccine
                else:
                    #add variant as list in Edge[0]
                    self.__Edges[0].append([lineList[0]])
                    #add vaccine as String in Edge[1]
                    self.__Edges[1].append(item)
            #if item already exists in the list 
            else:
                index= self.__VaccineList.index(item)
                #if item is variant
                if lineList[0] is item:
                    for j in lineList[1:]:
                        #if vaccines related to variant doen't exists in the list then append it in the list
                        if type(self.__Edges[1][index]) is list:
                            if j not in self.__Edges[1][index]:
                                __Edges[1][index].append(j)
                        else:
                            print("Exception!! index is not list")
                #if item is vaccine
                else:
                    if type(self.__Edges[0][index]) is list:
                        #check if variant is not in list showing mapping of vaccine to its variants then add vaccine in the list 
                        if lineList[0] not in self.__Edges[0][index]:
                            self.__Edges[0][index].append(lineList[0])
                    else:
                        print("Exception!! index is not list")
    
    #To read input file and add entries into the graph
    def readInputfile(self, inputfile):
        #open input file with read mode
        inputFile = open(inputfile, "r")
        #read lines and create list of lines
        inputs = inputFile.readlines()
        #split each line into list of vertices and add items into the graph
        for line in inputs:
            lineList = line.strip().split('/')
            resultList = [item.strip() for item in lineList if item.strip()]
            self.__addInput(resultList)
        inputFile.close()
    
    #function to display all the strains and vaccines details 
    def displayAll(self):
        vaccines = []
        strains = []
        for index in range(len(self.__VaccineList)):
            #Edges[0] store all the strains, so item with type string give distinct strains
            if type(self.__Edges[0][index]) is str: 
                strains.append(self.__Edges[0][index])
            #Edges[1] store all the vaccines, so item with type string give distinct vaccines
            if type(self.__Edges[1][index]) is str: 
                vaccines.append(self.__Edges[1][index])
        outputFile = open("outputPS16.txt", "a")
        outputFile.write("\n--------Function displayAll--------\n")
        #Print no. of strains in the graph
        outputFile.write("Total no. of strains:")
        outputFile.write(str(len(strains)))
        outputFile.write("\n")
        #Print no. of vaccines in the graph
        outputFile.write("Total no. of vaccines:")
        outputFile.write(str(len(vaccines)))
        outputFile.write("\n")
        #Print strains in the graph
        outputFile.write("List of strains:\n")
        for item in strains:
            outputFile.write(item)
            outputFile.write("\n")
        outputFile.write("\nList of vaccines:\n")
        #Print vaccines in the graph
        for item in vaccines:
            outputFile.write(item)
            outputFile.write("\n")
        outputFile.write("."*30)
        outputFile.close()
            
    #function to display all the strains for the given vaccine
    def displayStrains(self, vaccine):
        outputFile = open("outputPS16.txt", "a")
        outputFile.write("\n--------Function displayStrain --------\n")
        outputFile.write("Vaccine name:")
        outputFile.write(vaccine)
        outputFile.write("\n")
        #check whether node exist or not in the graph and get index
        if vaccine in self.__VaccineList:
            index = self.__VaccineList.index(vaccine)
            #get Strains list for given vaccine
            strains = self.__Edges[0][index]
            #if given is not a vaccine but strain
            if type(strains) is not list:
                outputFile.write(vaccine)
                outputFile.write( " is not a vaccine.\n")
                outputFile.write("."*30)
                return 0
            #Print all the Strains from the list
            outputFile.write("List of Strains:\n")
            for item in strains:
                outputFile.write(item)
                outputFile.write("\n")
        #if node does not exist in the VaccineList
        else:
            outputFile.write("This vaccine with the given name doesn't exist in our database\n")
        outputFile.write("."*30)
        outputFile.close()
        
    #function to display all the vaccines associated to the strain
    def displayVaccine(self, strain):
        outputFile = open("outputPS16.txt", "a")
        outputFile.write("\n--------Function displayVaccine --------\n")
        outputFile.write("Strain name:")
        outputFile.write(strain)
        outputFile.write("\n")
        #check whether node exist or not in the graph and get index
        if strain in self.__VaccineList:
            index = self.__VaccineList.index(strain)
            #get Vaccine list for given vaccine
            vaccines = self.__Edges[1][index]
            #if given is not a strain but vaccine
            if type(vaccines) is not list:
                outputFile.write(strain)
                outputFile.write( " is not a strain.\n")
                outputFile.write("."*30)
                return 0
            #Print all the Vaccines from the list
            outputFile.write("List of Vaccines:\n")
            for item in vaccines:
                outputFile.write(item)
                outputFile.write("\n")
        #if node does not exist in the VaccineLis
        else:
            outputFile.write("This strain with the given name doesn't exist in our database\n")
        outputFile.write("."*30)
        outputFile.close()
        
    #function returns intersection of two list
    def __intersection(self,lst1, lst2):
        lst3 = [value for value in lst1 if value in lst2]
        return lst3
    #function to find the common strains between two vaccines
    def commonStrain(self, vacA, vacB):
        outputFile = open("outputPS16.txt", "a")
        outputFile.write("\n--------Function commonStrain --------\n")
        outputFile.write("Vaccine A:")
        outputFile.write(vacA)
        outputFile.write("\n")
        outputFile.write("Vaccine B:")
        outputFile.write(vacB)
        outputFile.write("\n")
        #Check if vacA is a vertix and get its index
        if vacA in self.__VaccineList:
            indexA = self.__VaccineList.index(vacA)
        else:
            outputFile.write("Vaccine A doesn't exist in our database.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        #Check if VacB is a vertix and get its index
        if vacB in self.__VaccineList:
            indexB = self.__VaccineList.index(vacB)
        else:
            outputFile.write("Vaccine B doesn't exist in our database.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        #getting Strain list for the given vaccines
        strainListA = self.__Edges[0][indexA]
        strainListB = self.__Edges[0][indexB]
        #Checking if VacA is valid Vaccine or a strain and return valid message if it is a Strain
        if type(strainListA) is not list:
            outputFile.write(vacA)
            outputFile.write( " is not a vaccine.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        #Checking if VacB is valid Vaccine or a Strain and return valid message if it is a Strain
        elif type(strainListB) is not list:
            outputFile.write(vacB)
            outputFile.write( " is not a vaccine.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        #Find intersection of both Strain list
        commonStrain = self.__intersection(strainListA,strainListB)
        #if there is no intersection
        if len(commonStrain) == 0:
            outputFile.write("There is no common Strain between these two vaccines\n")
            outputFile.write("."*30)
            outputFile.close()
        #if there are some common Strains
        else:
            outputFile.write("common strain: Yes,")
            outputFile.write(','.join(commonStrain))
            outputFile.write("\n")
            outputFile.write("."*30)
            outputFile.close()
            
    #global variable to store all the retrieved paths from printAllPathsUtil method
    __paths=[]
    
    #function to store path into the paths list
    def __addPath(self,path:list):
        #joining path by delimiter >
        self.__paths.append(' > '.join(path))
            
    #recursive function to find all the paths between u and d        
    def printAllPathsUtil(self, u, d, visited, path):
        visited[self.__VaccineList.index(u)]= True
        path.append(u)
        #Condition if path length exceeds 5 because we just want to get path via 1 common vaccine
        if len(path)>5:
            path.pop()
            visited[self.__VaccineList.index(u)]= False
        else:
            #if we reach the destination and our path have just one common vaccine
            if u == d and len(path)==5:
                #add path into the global paths list
                self.__addPath(path)
            else:
                #to get the list of the items node is associated to if it is a vaccine
                adj = self.__Edges[0][self.__VaccineList.index(u)]
                #to get the list of the items node is associated to if it is a strain
                if type(adj) is not list:
                    adj = self.__Edges[1][self.__VaccineList.index(u)]
                #for each adjacent node call printAllPathsUtil if the node is not already visited
                for i in adj:
                    if visited[self.__VaccineList.index(i)]== False:
                        self.printAllPathsUtil(i, d, visited, path)
            #remove the node from the path
            path.pop()
            #marking the node as not visited for more iterations 
            visited[self.__VaccineList.index(u)]= False
              
    #function to get path between vacA and vacB via only 1 common vacC      
    def findVaccineConnect(self, vacA, vacB): 
        outputFile = open("outputPS16.txt", "a")
        outputFile.write("\n----------------Function findVaccineConnect --------\n")
        outputFile.write("Vaccine A:")
        outputFile.write(vacA)
        outputFile.write("\n")
        outputFile.write("Vaccine B:")
        outputFile.write(vacB)
        outputFile.write("\n")
        #Checking whether vacA is a valid vertix in the graph
        if vacA not in self.__VaccineList:
            outputFile.write("Vaccine A doesn't exist in our database.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        #Checking whether vacB is a valid vertix in the graph
        if vacB not in self.__VaccineList:
            outputFile.write("Vaccine B doesn't exist in our database.\n")
            outputFile.write("."*30)
            outputFile.close()
            return 0
        outputFile.write("Related: ")
        #Setting global path as null list to remove all the items from previous function calls
        self.__paths=[]
        #creating visited list to track whether the node is visited or not
        visited=[False]*len(self.__VaccineList)
        #creating null path list to store current path
        path=[]
        #calling printAllPathsUtil to get all the valid paths between vacA and vacB
        self.printAllPathsUtil(vacA, vacB, visited, path)
        #if no valid path is found
        if len(self.__paths) == 0:
            outputFile.write("No, They do not have any connection in between\n")
        #if valid path is found, then print it into the file
        else:
            outputFile.write("Yes,\n")
            for item in self.__paths:
                outputFile.write(item)
                outputFile.write("\n") 
        outputFile.write("."*30)
        outputFile.close()
        
    # Function to Parse the Prompt File 
    def parsePromptFile(self,promptFile):
        inputFile = open(promptFile,"r")
        inputs = inputFile.readlines()
        for line in inputs:
            #Split line into a list
            lineList = line.strip().split(':')
            #if user want to find list of strains for given vaccine
            if lineList[0].strip() == "findStrain":
                self.displayStrains(lineList[1].strip())
            #if user want to find list of vaccines for given strain
            elif lineList[0].strip() == "listVaccine":
                self.displayVaccine(lineList[1].strip())
            #if user want to find common Strains for given vaccines
            elif lineList[0].strip() == "commonStrain":
                self.commonStrain(lineList[1].strip(),lineList[2].strip())
            #if user want to find path two vaccines are related to each other through a common vaccine 
            elif lineList[0].strip() == "vaccineConnect":
                self.findVaccineConnect(lineList[1].strip(),lineList[2].strip()) 
            
#inistialize IMMUNIZATION object 
obj=IMMUNIZATION()
if Path("inputPS16.txt").is_file():
    #read input file and creating graph according to the given input
    obj.readInputfile("inputPS16.txt")
    #display no. of distinct strains and vaccines and listing them
    obj.displayAll()
    if Path("promptsPS16.txt").is_file():
        #parse prompt file
        obj.parsePromptFile("promptsPS16.txt")
    else:
        print("Prompt file is not available on the path given.")
else:
    print("Input file is not available on the path given.")
print("done")

